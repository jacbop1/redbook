val scala3Version = "3.1.2"

lazy val root = project
  .in(file("."))
  .settings(
    name := "redbook",
    version := "0.3.0-SNAPSHOT",
    scalaVersion := scala3Version,
    libraryDependencies += "org.scalameta" %% "munit" % "0.7.29" % Test
  )
