package chapter4

sealed trait Option[+A]:
  def map[B](f: A => B): Option[B]
  def flatMap[B](f: A => Option[B]): Option[B]
  def getOrElse[B >: A](default: => B): B
  def orElse[B >: A](ob: => Option[B]): Option[B]
  def filter(f: A => Boolean): Option[A]

case class Some[+A](get: A) extends Option[A]:
  override def map[B](f: A => B): Option[B] = Some(f(get))
  override def flatMap[B](f: A => Option[B]): Option[B] = f(get)
  override def getOrElse[B >: A](default: => B): B = get
  override def orElse[B >: A](ob: => Option[B]): Option[B] = Some(get)
  override def filter(f: A => Boolean): Option[A] =
    if f(get) then Some(get) else None

case object None extends Option[Nothing]:
  override def map[B](f: Nothing => B): Option[B] = None
  override def flatMap[B](
      f: Nothing => Option[B]
  ): Option[Nothing] = None
  override def getOrElse[B >: Nothing](default: => B): B = default
  override def orElse[B >: Nothing](ob: => Option[B]): Option[B] = ob
  override def filter(f: Nothing => Boolean): Option[Nothing] = None

object Option:
  def mean(xs: Seq[Double]): Option[Double] =
    if xs.isEmpty then None
    else Some(xs.sum / xs.length)

  def variance(xs: Seq[Double]): Option[Double] =
    mean(xs).flatMap(m => mean(xs.map(x => math.pow(x - m, 2.0))))

  def lift[A, B](f: A => B): Option[A] => Option[B] = _ map f

  val abs0: Option[Double] => Option[Double] = lift(math.abs)

  def map2[A, B, C](ao: Option[A], bo: Option[B])(f: (A, B) => C): Option[C] =
    ao.flatMap(a => bo.map(b => f(a, b)))

  def sequence[A](aos: List[Option[A]]): Option[List[A]] =
    aos.foldRight[Option[List[A]]](Some(Nil))((ao, acc) =>
      ao.flatMap(a => acc.flatMap(as => Some(a +: as)))
    )

  def traverseS[A, B](as: List[A])(f: A => Option[B]): Option[List[B]] =
    sequence(as.map(f))

  def traverse[A, B](as: List[A])(f: A => Option[B]): Option[List[B]] =
    as.foldRight[Option[List[B]]](Some(Nil))((a, acc) =>
      f(a).flatMap(b => acc.flatMap(bs => Some(b +: bs)))
    )

  def sequenceT[A](aos: List[Option[A]]): Option[List[A]] =
    traverse(aos)(x => x)
