package chapter4

sealed trait Validated[+E, +A]:
  def map[B](f: A => B): Validated[E, B] = this match
    case Valid(a)   => Valid(f(a))
    case Invalid(e) => Invalid(e)
  def flatMap[EE >: E, B](f: A => Validated[EE, B]): Validated[EE, B] =
    this match
      case Valid(a)   => f(a)
      case Invalid(e) => Invalid(e)
  def orElse[EE >: E, B >: A](b: => Validated[EE, B]): Validated[EE, B] =
    this match
      case Valid(a)   => Valid(a)
      case Invalid(e) => b
  def map2[EE >: E, B, C](
      mb: Validated[EE, B]
  )(f: (A, B) => C): Validated[EE, C] =
    (this, mb) match {
      case (Valid(a), Valid(b))         => Valid(f(a, b))
      case (Invalid(es), Valid(_))      => Invalid(es)
      case (Valid(_), Invalid(es))      => Invalid(es)
      case (Invalid(esa), Invalid(esb)) => Invalid(esa ++ esb)
    }

case class Invalid[+E](values: List[E]) extends Validated[E, Nothing]
case class Valid[+A](value: A) extends Validated[Nothing, A]

sealed trait ValidatedG[+E, +A]:
  def map[B](f: A => B): ValidatedG[E, B] = this match
    case ValidG(a)   => ValidG(f(a))
    case InvalidG(e) => InvalidG(e)
  def flatMap[EE >: E, B](f: A => ValidatedG[EE, B]): ValidatedG[EE, B] =
    this match
      case ValidG(a)   => f(a)
      case InvalidG(e) => InvalidG(e)
  def orElse[EE >: E, B >: A](b: => ValidatedG[EE, B]): ValidatedG[EE, B] =
    this match
      case ValidG(a)   => ValidG(a)
      case InvalidG(e) => b
  def map2[EE >: E, B, C](
      mb: ValidatedG[EE, B]
  )(f: (A, B) => C)(g: (EE, EE) => EE): ValidatedG[EE, C] =
    (this, mb) match {
      case (ValidG(a), ValidG(b))         => ValidG(f(a, b))
      case (InvalidG(es), ValidG(_))      => InvalidG(es)
      case (ValidG(_), InvalidG(es))      => InvalidG(es)
      case (InvalidG(esa), InvalidG(esb)) => InvalidG(g(esa, esb))
    }

case class InvalidG[+E](value: E) extends ValidatedG[E, Nothing]
case class ValidG[+A](value: A) extends ValidatedG[Nothing, A]

object ValidatedG:
  def sequence[E, A](es: List[ValidatedG[E, A]]): ValidatedG[E, List[A]] =
    traverse(es)(a => a)
  def traverse[E, A, B](
      as: List[A]
  )(f: A => ValidatedG[E, B]): ValidatedG[E, List[B]] =
    as.foldRight[ValidatedG[E, List[B]]](ValidG(Nil))((a, acc) =>
      f(a).flatMap(b => acc.flatMap(bs => ValidG(b +: bs)))
    )
