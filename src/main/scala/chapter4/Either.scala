package chapter4

sealed trait Either[+E, +A]:
  def map[B](f: A => B): Either[E, B]
  def flatMap[EE >: E, B](f: A => Either[EE, B]): Either[EE, B]
  def orElse[EE >: E, B >: A](b: => Either[EE, B]): Either[EE, B]
  def map2[EE >: E, B, C](mb: Either[EE, B])(f: (A, B) => C): Either[EE, C] =
    for {
      a <- this
      b <- mb
      c = f(a, b)
    } yield c

case class Left[+E](value: E) extends Either[E, Nothing]:
  override def map[B](f: Nothing => B): Either[E, B] = Left(value)
  override def flatMap[EE >: E, B](f: Nothing => Either[EE, B]): Either[EE, B] =
    Left(
      value
    )
  override def orElse[EE >: E, B](
      b: => Either[EE, B]
  ): Either[EE, B] =
    b

case class Right[+A](value: A) extends Either[Nothing, A]:
  override def map[B](f: A => B): Either[Nothing, B] = Right(f(value))
  override def flatMap[Nothing, B](
      f: A => Either[Nothing, B]
  ): Either[Nothing, B] = f(value)
  override def orElse[Nothing, B >: A](
      b: => Either[Nothing, B]
  ): Either[Nothing, B] =
    Right(value)

object Either:
  def sequence[E, A](es: List[Either[E, A]]): Either[E, List[A]] =
    traverse(es)(a => a)
  def traverse[E, A, B](as: List[A])(f: A => Either[E, B]): Either[E, List[B]] =
    as.foldRight[Either[E, List[B]]](Right(Nil))((a, acc) =>
      f(a).flatMap(b => acc.flatMap(bs => Right(b +: bs)))
    )
