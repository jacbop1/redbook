package chapter2
/*
 * implement a higher-order function that composes two functions
 */
object Exercise2_5:

  def compose[A, B, C](f: B => C, g: A => B): A => C =
    a => f(g(a))
