package chapter2
/*
 * implement `isSorted`, which checks whether
 * and `Array[A]` is sorted according to a
 * given comparison function
 */
object Exercise2_2:
  def isSorted[A](as: Array[A], ordered: (A, A) => Boolean): Boolean =

    @annotation.tailrec
    def go(n: Int): Boolean =
      if n >= as.length - 1 then true
      else if !ordered(as(n), as(n + 1)) then false
      else go(n + 1)

    go(0)
