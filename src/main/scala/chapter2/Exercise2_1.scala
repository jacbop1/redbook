package chapter2
/*
 * write a recursive function to get the nth fibonacci number.
 * definition should use a local tail recursive function.
 */
object Exercise2_1:
  def fibonacci(n: Int): Int =

    require(n > 0)

    @annotation.tailrec
    def go(n: Int, prev: Int, curr: Int): Int =
      if n == 0 then prev
      else go(n - 1, curr, prev + curr)

    go(n - 1, 0, 1)
