package chapter2
/*
 * implement uncurry function that reverses the transformation of curry
 */
object Exercise2_4:

  def uncurry[A, B, C](f: A => B => C): (A, B) => C =
    (a: A, b: B) => f(a)(b)
