package chapter2
/*
 * implement a curry function that converts a function of two
 * arguments to a function of one argument that partially applies
 */
object Exercise2_3:

  def curry[A, B, C](f: (A, B) => C): A => (B => C) =
    ((a: A) => (b: B) => f(a, b))
