package chapter3

/** Hard: Can you write `foldLeft` in terms of `foldRight`? How about the other
  * way around?
  */
trait Exercise3_13
