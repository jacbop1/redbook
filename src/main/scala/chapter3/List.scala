package chapter3

sealed trait List[+A]
case object Nil extends List[Nothing]
case class Cons[+A](head: A, tail: List[A]) extends List[A]

object List:
  def sum(ints: List[Int]): Int = ints match
    case Nil         => 0
    case Cons(x, xs) => x + sum(xs)

  def product(ds: List[Double]): Double = ds match
    case Nil          => 1.0
    case Cons(0.0, _) => 0.0
    case Cons(x, xs)  => x * product(xs)

  def apply[A](as: A*): List[A] =
    if as.isEmpty then Nil
    else Cons(as.head, apply(as.tail: _*))

  def tail[A](as: List[A]): List[A] = as match
    case Nil        => throw IllegalArgumentException("Nil has no tail")
    case Cons(_, t) => t

  def setHead[A](as: List[A], h: A): List[A] = as match
    case Nil        => List(h)
    case Cons(_, t) => Cons(h, t)

  def drop[A](as: List[A], n: Int): List[A] = (n, as) match
    case (0, l)          => l
    case (_, Nil)        => Nil
    case (x, Cons(_, t)) => drop(t, x - 1)

  def dropWhile[A](as: List[A], f: A => Boolean): List[A] = as match
    case Cons(h, t) if f(h) => dropWhile(t, f)
    case _                  => as

  def appendA[A](l: List[A], a: A): List[A] = l match
    case Nil        => List(a)
    case Cons(h, t) => Cons(h, appendA(t, a))

  def append[A](a1: List[A], a2: List[A]): List[A] = a1 match
    case Nil        => a2
    case Cons(h, t) => Cons(h, append(t, a2))

  def init[A](l: List[A]): List[A] = l match
    case Nil          => throw IllegalArgumentException("Nil has no init")
    case Cons(_, Nil) => Nil
    case Cons(h, t)   => Cons(h, init(t))

  def foldRight[A, B](l: List[A], z: B)(f: (A, B) => B): B = l match
    case Nil        => z
    case Cons(h, t) => f(h, foldRight(t, z)(f))

  def sum2(ns: List[Int]) = foldRight(ns, 0) { _ + _ }
  def product2(ns: List[Double]) = foldRight(ns, 0.0) { _ * _ }

  def length[A](l: List[A]): Int = foldRight(l, 0) { (_, n) =>
    n + 1
  }

  @annotation.tailrec
  def foldLeft[A, B](l: List[A], z: B)(f: (B, A) => B): B = l match
    case Nil        => z
    case Cons(h, t) => foldLeft(t, f(z, h))(f)

  def sumL(l: List[Int]): Int =
    foldLeft(l, 0)(_ + _)

  def productL(l: List[Double]): Double =
    foldLeft(l, 1.0)(_ * _)

  def size[A](l: List[A]): Int = foldLeft(l, 0) { (n, _) =>
    n + 1
  }

  def reverse[A](l: List[A]): List[A] = foldLeft(l, Nil: List[A]) { (acc, h) =>
    Cons(h, acc)
  }

  def foldLeftRVerbose[A, B](l: List[A], z: B)(f: (B, A) => B): B =
    val identity = (z: B) => z
    val delayedCombiner = (a: A, g: B => B) => (b: B) => g(f(b, a))
    foldRight(l, identity)(delayedCombiner)(z)

  def foldLeftR[A, B](l: List[A], z: B)(f: (B, A) => B): B =
    foldRight(l, (z: B) => z)((a, g) => b => g(f(b, a)))(z)

  def foldRightL[A, B](l: List[A], z: B)(f: (A, B) => B): B =
    foldLeft(l, (z: B) => z)((g, a) => b => g(f(a, b)))(z)

  def appendF[A](l: List[A], r: List[A]): List[A] =
    foldRightL(l, r)(Cons(_, _))

  def concat[A](ll: List[List[A]]): List[A] =
    foldRightL(ll, Nil: List[A])(appendF)

  def embiggen(ints: List[Int]): List[Int] =
    foldRightL(ints, Nil: List[Int])((i, acc) => Cons(i + 1, acc))

  def dbl2str(ds: List[Double]): List[String] =
    foldRightL(ds, Nil: List[String])((d, acc) => Cons(d.toString(), acc))

  def map[A, B](as: List[A])(f: A => B): List[B] =
    foldRight(as, Nil: List[B])((a, acc) => Cons(f(a), acc))

  def filter[A](l: List[A])(f: A => Boolean): List[A] =
    foldRight(l, Nil: List[A]) { (h, t) =>
      if f(h) then Cons(h, t)
      else t
    }

  def flatMap[A, B](as: List[A])(f: A => List[B]): List[B] =
    concat(map(as)(a => f(a)))

  def filterFM[A](l: List[A])(f: A => Boolean): List[A] =
    flatMap(l)(a => if f(a) then List(a) else Nil)

  def zipAdd(xs: List[Int], ys: List[Int]): List[Int] =
    (xs, ys) match {
      case (Nil, _)                   => Nil
      case (_, Nil)                   => Nil
      case (Cons(x, xt), Cons(y, yt)) => Cons(x + y, zipAdd(xt, yt))
    }

  def zipWith[A, B](xs: List[A], ys: List[A])(f: (A, A) => B): List[B] =
    (xs, ys) match {
      case (Nil, _) => Nil
      case (_, Nil) => Nil
      case (Cons(x, xt), Cons(y, yt)) =>
        Cons(f(x, y), zipWith(xt, yt)(f))
    }

  def hasSubsequence[A](sup: List[A], sub: List[A]): Boolean =
    @annotation.tailrec
    def check(sp: List[A], sb: List[A], sub: List[A]): Boolean =
      (sp, sb, sub) match {
        case (_, Nil, _) => true
        case (Nil, _, _) => false
        case (Cons(hp, tp), Cons(hb, tb), sub) =>
          if (hp != hb)
            if (sb == sub)
              check(tp, sub, sub)
            else
              check(sp, sub, sub)
          else
            check(tp, tb, sub)
      }
    check(sup, sub, sub)
