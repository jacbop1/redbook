package chapter3

/** Implement `dropWhile`, which removes an element from the List prefix as long
  * as they match a predicate
  */
trait Exercise3_5
