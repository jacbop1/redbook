package chapter3

/** What happens when you pass `Nil` and `Cons` themselves to `foldRight`? What
  * does this say about the relationship between `foldRight` and the data
  * constructor of `List`?
  */
trait Exercise3_8
