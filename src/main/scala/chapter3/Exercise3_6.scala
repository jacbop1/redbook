package chapter3

/** Implement a function, `init`, that returns a `List` consisting of all but
  * the last element of a List.
  */
trait Exercise3_6
