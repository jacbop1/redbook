package chapter3

/** Write a function `depth` that returns the maximum path length from the root
  * of the tree to any leaf
  */
trait Exercise3_27
