package chapter3

/** Implement the function `setHead` for replacing the first element of a List
  * with a different value.
  */
trait Exercise3_3
