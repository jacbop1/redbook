package chapter3

/** Generalize `tail` to the function `drop`, which removes the first `n`
  * elements from a list.
  */
trait Exercise3_4
