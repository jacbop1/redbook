package chapter3

/** Can `product`, implemented using `foldRight`, immediately halt the recursion
  * and return 0.0 if it encounters a 0.0?
  */
trait Exercise3_7
