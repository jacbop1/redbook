package chapter3

sealed trait Tree[+A]
case class Leaf[A](value: A) extends Tree[A]
case class Branch[A](left: Tree[A], right: Tree[A]) extends Tree[A]

object Tree:
  def size[A](t: Tree[A]): Int = t match
    case Leaf(_)      => 1
    case Branch(l, r) => 1 + size(l) + size(r)

  def maximum(is: Tree[Int]): Int = is match
    case Leaf(i)      => i
    case Branch(l, r) => maximum(l) max maximum(r)

  def depth[A](is: Tree[A]): Int = is match
    case Leaf(_)      => 0
    case Branch(l, r) => 1 + depth(l) max depth(r)

  def map[A, B](as: Tree[A])(f: A => B): Tree[B] = as match
    case Leaf(a)      => Leaf(f(a))
    case Branch(l, r) => Branch(map(l)(f), map(r)(f))

  def fold[A, B](as: Tree[A])(f: A => B)(sum: (B, B) => B): B =
    as match
      case Leaf(a)      => f(a)
      case Branch(l, r) => sum(fold(l)(f)(sum), fold(r)(f)(sum))

  def sizeF[A](t: Tree[A]): Int = fold(t)(_ => 1)(1 + _ + _)

  def maximumF(t: Tree[Int]): Int = fold(t)(i => i)(_ max _)

  def depthF[A](t: Tree[A]): Int = fold(t)(i => 0)((x, y) => 1 + (x max y))

  def mapF[A, B](t: Tree[A])(f: A => B): Tree[B] =
    fold(t)(a => Leaf(f(a)): Tree[B])(Branch(_, _))
