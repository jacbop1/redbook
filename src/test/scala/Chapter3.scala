class Chapter3 extends munit.FunSuite:

  import chapter3.*

  test("exercise 3.1") {
    import chapter3.Exercise3_1.x
    assertEquals(x, 3)
  }

  test("exercise 3.2") {
    intercept[IllegalArgumentException](List.tail(Nil))
    assertEquals(List.tail(List(1, 2, 3)), List(2, 3))
  }

  test("exercise 3.3") {
    assertEquals(List.setHead(Nil, 9), List(9))
    assertEquals(List.setHead(List(1, 2, 3), 9), List(9, 2, 3))
  }

  test("exercise 3.4") {
    assertEquals(List.drop(Nil, 999), Nil)
    assertEquals(List.drop(List(1, 2, 3), 2), List(3))
    assertEquals(List.drop(List(1, 2, 3), 3), Nil)
    assertEquals(List.drop(List(1, 2, 3), 4), Nil)
    assertEquals(List.drop(List(1, 2, 3), 999), Nil)
  }

  test("exercise 3.5") {
    assertEquals(List.dropWhile(List(1, 2, 3), x => x <= 2), List(3))
    assertEquals(List.dropWhile(List(1, 2, 3), x => x <= 100), Nil)
    assertEquals(List.dropWhile[Int](Nil, x => x <= 100), Nil)
  }

  test("exercise 3.5.experiment") {
    assertEquals(List.appendA(List(1, 2, 3), 9), List(1, 2, 3, 9))
    assertEquals(List.append(List(1, 2, 3), Nil), List(1, 2, 3))
    assertEquals(List.append(List(1, 2, 3), List(9)), List(1, 2, 3, 9))
    assertEquals(List.append(Nil, List(9)), List(9))
  }

  test("exercise 3.6") {
    intercept[IllegalArgumentException](List.init(Nil))
    assertEquals(List.init(List(1)), Nil)
    assertEquals(List.init(List(1, 2, 3)), List(1, 2))
  }

  test("exercise 3.7") {
    assertEquals(List.product2(List(1.0, 0.0, 3.0)), 0.0)
    // cannot short circuit when we encounter a zero with this implementation: all the control flow is in foldRight
  }

  test("exercise 3.8 is identity function when using z=Nil and f=Cons") {
    assertEquals(
      List.foldRight(List(1, 2, 3), Nil: List[Int])(Cons(_, _)),
      List(1, 2, 3)
    )
  }

  test("exercise 3.9") {
    assertEquals(List.length(List(1.0, 2.0, 3.0)), 3)
    assertEquals(List.length(List(1, 2, 3)), 3)
    assertEquals(List.length(Nil), 0)
  }

  test("exercise 3.10") {
    val v = List.foldLeft(List(1, 2, 3, 4, 5), 0)(_ + _)
    assertEquals(v, 15)
  }

  test("exercise 3.11") {
    assertEquals(List.sumL(List(1, 2, 3)), 6)
    assertEquals(List.productL(List(1.0, 2.0, 3.0)), 6.0)
    assertEquals(List.size(List(1.0, 2.0, 3.0)), 3)
  }

  test("exercise 3.12") {
    assertEquals(List.reverse(List(1, 2, 3)), List(3, 2, 1))
  }

  test("exercise 3.13") {
    val fl = List.foldLeftR(List("a", "b", "c"), "^")((i, j) => s"${i}${j}")
    assertEquals(fl, "^abc")
    val fr = List.foldRightL(List("a", "b", "c"), "$")((j, i) => s"${j}${i}")
    assertEquals(fr, "abc$")
  }

  test("exercise 3.14") {
    assertEquals(List.appendF(Nil, List(9)), List(9))
    assertEquals(List.appendF(List(1, 2, 3), List(9)), List(1, 2, 3, 9))
  }

  test("exercise 3.15") {
    assertEquals(
      List.concat(List(List(1, 2, 3), List(4, 5, 6))),
      List(1, 2, 3, 4, 5, 6)
    )
  }

  test("exercise 3.16") {
    assertEquals(
      List.embiggen(List(1, 2, 3)),
      List(2, 3, 4)
    )
  }

  test("exercise 3.17") {
    assertEquals(
      List.dbl2str(List(1.1, 2.2, 3.3)),
      List("1.1", "2.2", "3.3")
    )
  }

  test("exercise 3.18") {
    assertEquals(
      List.map(List(1, 2, 3))(_.toString()),
      List("1", "2", "3")
    )
    assertEquals(
      List.map(List(1, 2, 3))(_ * 2),
      List(2, 4, 6)
    )
  }

  test("exercise 3.19") {
    assertEquals(List.filter(List(1, 2, 3))(_ % 2 == 0), List(2))
  }

  test("exercise 3.20") {
    assertEquals(
      List.flatMap(List(1, 2, 3))(i => List(i, i)),
      List(1, 1, 2, 2, 3, 3)
    )
  }

  test("exercise 3.21") {
    assertEquals(List.filterFM(List(1, 2, 3))(_ % 2 == 0), List(2))
  }

  // todo scalacheck to try various lists, which property?
  test("exercise 3.22") {
    assertEquals(List.zipAdd(List(1, 2, 3), (List(4, 5, 6))), List(5, 7, 9))
    assertEquals(List.zipAdd(List(1, 2, 3, 9), (List(4, 5, 6))), List(5, 7, 9))
    assertEquals(List.zipAdd(List(1, 2, 3), (List(4, 5, 6, 9))), List(5, 7, 9))
  }

  test("exercise 3.23") {
    assertEquals(
      List.zipWith[String, String](List("a", "b", "c"), List("4", "5", "6"))(
        (x, y) => x + y
      ),
      List("a4", "b5", "c6")
    )
  }

  test("exercise 3.24") {
    assert(List.hasSubsequence(List(1, 2, 3, 4), Nil))
    assert(List.hasSubsequence(List(1, 2, 3, 4), List(2, 3)))
    assert(List.hasSubsequence(List(1, 2, 2, 3, 4), List(2, 3)))
    assert(!List.hasSubsequence(List(1, 2, 3, 4), List(2, 4)))
  }

  test("exercise 3.25") {
    assertEquals(Tree.size(Branch(Branch(Leaf(1), Leaf(2)), Leaf(3))), 5)
  }

  test("exercise 3.26") {
    assertEquals(Tree.maximum(Branch(Branch(Leaf(1), Leaf(2)), Leaf(3))), 3)
    assertEquals(Tree.maximum(Branch(Branch(Leaf(3), Leaf(2)), Leaf(1))), 3)
    assertEquals(Tree.maximum(Leaf(5)), 5)
  }

  test("exercise 3.27") {
    assertEquals(Tree.depth(Branch(Branch(Leaf(1), Leaf(2)), Leaf(3))), 2)
    assertEquals(Tree.depth(Branch(Leaf(3), Leaf(1))), 1)
    assertEquals(Tree.depth(Leaf(5)), 0)
  }

  test("exercise 3.28") {
    assertEquals(
      Tree.map[Int, String](
        Branch(left = Branch(Leaf(1), Leaf(2)), right = Leaf(3))
      )(_.toString),
      Branch(Branch(Leaf("1"), Leaf("2")), Leaf("3"))
    )
    assertEquals(
      Tree.map[Int, Double](
        Branch(left = Branch(Leaf(1), Leaf(2)), right = Leaf(3))
      )(_ * 2.0),
      Branch(Branch(Leaf(2.0), Leaf(4.0)), Leaf(6.0))
    )
  }

  test("exercise 3.29") {
    assertEquals(
      Tree.fold[Int, String](
        Branch(left = Branch(Leaf(1), Leaf(2)), right = Leaf(3))
      )((a) => a.toString)((a, b) => a concat b),
      "123"
    )
    assertEquals(
      Tree.fold[Int, Double](
        Branch(left = Branch(Leaf(1), Leaf(2)), right = Leaf(3))
      )((a) => a * 2)((a, b) => a + b),
      12.0
    )

    assertEquals(Tree.sizeF(Branch(Branch(Leaf(1), Leaf(2)), Leaf(3))), 5)

    assertEquals(Tree.maximumF(Branch(Branch(Leaf(1), Leaf(2)), Leaf(3))), 3)
    assertEquals(Tree.maximumF(Branch(Branch(Leaf(3), Leaf(2)), Leaf(1))), 3)
    assertEquals(Tree.maximumF(Leaf(5)), 5)

    assertEquals(Tree.depthF(Branch(Branch(Leaf(1), Leaf(2)), Leaf(3))), 2)
    assertEquals(Tree.depthF(Branch(Leaf(3), Leaf(1))), 1)
    assertEquals(Tree.depthF(Leaf(5)), 0)

    assertEquals(
      Tree.mapF[Int, String](
        Branch(left = Branch(Leaf(1), Leaf(2)), right = Leaf(3))
      )(_.toString),
      Branch(Branch(Leaf("1"), Leaf("2")), Leaf("3"))
    )
    assertEquals(
      Tree.mapF[Int, Double](
        Branch(left = Branch(Leaf(1), Leaf(2)), right = Leaf(3))
      )(_ * 2.0),
      Branch(Branch(Leaf(2.0), Leaf(4.0)), Leaf(6.0))
    )
  }
