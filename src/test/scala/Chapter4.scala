import org.junit.internal.runners.statements.Fail
class Chapter4 extends munit.FunSuite:

  import scala.util.{Try, Success, Failure}

  {
    import chapter4.{Option, Some, None}
    import chapter4.Option.*
    test("exercise 4.1") {
      assertEquals(Some(1).map(_.toString), Some("1"))
      assertEquals(None.map(_.toString), None)
      assertEquals(Some(1).flatMap(x => Some(x.toString)), Some("1"))
      assertEquals(None.flatMap(_ => None), None)
      assertEquals(Some(1).getOrElse(99), 1)
      assertEquals(None.getOrElse[Int](99), 99)
      assertEquals(Some(1).orElse(None), Some(1))
      assertEquals(None.orElse(Some(9)), Some(9))
      assertEquals(Some(1).filter(_.isWhole), Some(1))
      assertEquals(Some(1.333).filter(_.isWhole), None)
      assertEquals(None.filter(_ => true), None)
    }

    test("exercise 4.2") {
      assertEquals(mean(Seq()), None)
      assertEquals(mean(Seq(0.0)), Some(0.0))
      assertEquals(mean(Seq(2.0, 5.0, 15.0, 3.0)), Some(6.25))
      assertEquals(variance(Seq(2.0, 5.0, 15.0, 3.0)), Some(26.6875))
    }

    test("exercise 4.3") {
      assertEquals(
        map2(Some(2.0), None: Option[Double])((x, y) => x + y),
        None
      )
      assertEquals(map2(None: Option[Double], Some(3.0))((x, y) => x + y), None)
      assertEquals(map2(Some(2.0), Some(3.0))((x, y) => x + y), Some(5.0))
    }

    test("exercise 4.4") {
      assertEquals(
        sequence(List(Some(1), Some(2), Some(3))),
        Some(List(1, 2, 3))
      )
      assertEquals(
        sequence(List(Some(1), None, Some(3))),
        None
      )
    }

    test("exercise 4.5") {
      assertEquals(
        traverse(List(1, 2, 3))(x => Some(x * 2)),
        Some(List(2, 4, 6))
      )
      assertEquals(
        traverse(List(-1, 0, 1))(x =>
          Try(1 / x).match {
            case Success(y) => Some(y)
            case Failure(_) => None
          }
        ),
        None
      )

      assertEquals(
        sequenceT(List(Some(1), Some(2), Some(3))),
        Some(List(1, 2, 3))
      )
      assertEquals(
        sequenceT(List(Some(1), None, Some(3))),
        None
      )
    }
  }

  {
    import chapter4.{Either, Left, Right}
    import chapter4.Either.*
    test("exercise 4.6") {
      assertEquals(Right(2).map(_ * 2), Right(4))
      assertEquals(Right("2").map(_.toUpperCase()), Right("2"))
      val e: Either[String, Int] = Left("ouch")
      assertEquals(e.map(_ * 2), e)

      assertEquals(Right(2).flatMap(x => Right(x * 2)), Right(4))

      assertEquals(e.orElse(Right(999)), Right(999))

      assertEquals(Right(8).map2(e)((a: Int, b: Int) => a + b), Left("ouch"))
      assertEquals(
        Right(8).map2(Right(4))((a: Int, b: Int) => a + b),
        Right(12)
      )
    }

    test("exercise 4.7") {
      assertEquals(
        sequence(List(Right(1), Right(2), Right(3))),
        Right(List(1, 2, 3))
      )
      assertEquals(
        sequence(List(Right(1), Left("shoe"), Right(3))),
        Left("shoe")
      )
      assertEquals(
        sequence(List(Left("old"), Left("shoe"), Right(3))),
        Left("old")
      )

      assertEquals(
        traverse(List(1, 2, 3))(a => Right(a * 10)),
        Right(List(10, 20, 30))
      )
      assertEquals(
        traverse(List("1.0", "brown", "3.0"))(a =>
          Try(a.toDouble) match {
            case Success(x) => Right(x)
            case Failure(_) => Left(s"${a} is not a number")
          }
        ),
        Left("brown is not a number")
      )
      assertEquals(
        traverse(List("old", "shoe", "3"))(a =>
          Try(a.toDouble) match {
            case Success(x) => Right(x)
            case Failure(_) => Left(s"${a} is not a number")
          }
        ),
        Left("old is not a number")
      )
    }
  }

  {
    import chapter4.{Validated, Valid, Invalid}
    case class Person(name: Name, age: Age)
    sealed class Name(val value: String)
    sealed class Age(val value: Int)

    def mkName(name: String): Validated[String, Name] =
      if name == "" || name == null then Invalid(List("name is empty"))
      else Valid(new Name(name))

    def mkAge(age: Int): Validated[String, Age] =
      if age < 0 then Invalid(List("age is out of range"))
      else Valid(new Age(age))

    def mkPerson(name: String, age: Int): Validated[String, Person] =
      mkName(name).map2(mkAge(age))(Person(_, _))

    test("exercise 4.8") {
      assertEquals(
        mkPerson("Tom", 32).map(_.name.value),
        Valid("Tom")
      )
      assertEquals(
        mkPerson("", 32).map(_.name.value),
        Invalid(List("name is empty"))
      )
      assertEquals(
        mkPerson("", -2).map(_.name.value),
        Invalid(List("name is empty", "age is out of range"))
      )
      assertEquals(
        mkPerson("", -2).map(_.name.value).orElse(Valid("Sean")),
        Valid("Sean")
      )
      assertEquals(
        mkPerson("Tom", 32).flatMap(a => Valid(a.name.value)),
        Valid("Tom")
      )
      assertEquals(
        mkPerson("Tom", 32)
          .map2(mkPerson("Jake", 28))((a, b) => a.name.value + b.name.value),
        Valid("TomJake")
      )
    }

  }

  {
    import chapter4.{ValidatedG, ValidG, InvalidG}
    import chapter4.ValidatedG.*
    case class Person(name: Name, age: Age)
    sealed class Name(val value: String)
    sealed class Age(val value: Int)

    def mkName(name: String): ValidatedG[List[String], Name] =
      if name == "" || name == null then InvalidG(List("name is empty"))
      else ValidG(new Name(name))

    def mkAge(age: Int): ValidatedG[List[String], Age] =
      if age < 0 then InvalidG(List("age is out of range"))
      else ValidG(new Age(age))

    def mkPerson(name: String, age: Int): ValidatedG[List[String], Person] =
      mkName(name).map2(mkAge(age))(Person(_, _))(_ ++ _)

    test("exercise 4.8 general") {
      assertEquals(
        mkPerson("Tom", 32).map(_.name.value),
        ValidG("Tom")
      )
      assertEquals(
        mkPerson("", 32).map(_.name.value),
        InvalidG(List("name is empty"))
      )
      assertEquals(
        mkPerson("", -2).map(_.name.value),
        InvalidG(List("name is empty", "age is out of range"))
      )
      assertEquals(
        mkPerson("", -2).map(_.name.value).orElse(ValidG("Sean")),
        ValidG("Sean")
      )
      assertEquals(
        mkPerson("Tom", 32).flatMap(a => ValidG(a.name.value)),
        ValidG("Tom")
      )
      assertEquals(
        mkPerson("Tom", 32)
          .map2(mkPerson("Jake", 28))((a, b) => a.name.value + b.name.value)(
            _ ++ _
          ),
        ValidG("TomJake")
      )

      assertEquals(
        traverse(List(("abby", 1), ("bruce", 2), ("cassie", 3)))((n, a) =>
          mkPerson(n, a)
        ).map(_.map(_.name.value)),
        ValidG(List("abby", "bruce", "cassie"))
      )

      assertEquals(
        sequence(
          List(ValidG("abby", 1), ValidG("bruce", 2), ValidG("cassie", 3))
        ).map(_.map((n, _) => n)),
        ValidG(List("abby", "bruce", "cassie"))
      )
    }

  }
