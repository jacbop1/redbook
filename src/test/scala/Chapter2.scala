class Chapter2 extends munit.FunSuite:
  test("exercise 2.1") {
    import chapter2.Exercise2_1.fibonacci
    assertEquals(fibonacci(1), 0)
    assertEquals(fibonacci(2), 1)
    assertEquals(fibonacci(13), 144)
  }

  test("exercise 2.2") {
    import chapter2.Exercise2_2.isSorted
    assertEquals(isSorted(Array(3, 2, 1), (x, y) => x <= y), false)
    assertEquals(isSorted(Array(1, 2, 3), (x, y) => x <= y), true)
  }

  test("exercise 2.3") {
    import chapter2.Exercise2_3.curry
    val f = Math.max
    val g = curry(f)
    assertEquals(g(3)(5), 5)
    assertEquals(g(5)(3), 5)
  }

  test("exercise 2.4") {
    import chapter2.Exercise2_4.uncurry
    val g = (x: Int) => (y: Int) => Math.max(x, y)
    val f = uncurry(g)
    assertEquals(f(3, 5), 5)
    assertEquals(f(5, 3), 5)
  }

  test("exercise 2.5") {
    import chapter2.Exercise2_5.compose
    val f = Math.sin
    val g = Math.sqrt
    val k = compose[Double, Double, Double](f, g)
    assertEquals(k(1.234), 0.8960787774014021)
  }
